"""
SBo Dashboard
problems.py
Functions and data structures relating to problems

David Spencer 2018
See LICENCE for copyright information
"""

import sys
import os
import glob
import subprocess
import logging
import json
import datetime
import requests

import globals
import stats
import utils

#-------------------------------------------------------------------------------
# Problems[prgnam] = { [ "type":   "problem type",
#                        "desc":   "description text",
#                        "source": "source",
#                        "since":  "datestring",
#                        "status": "new/fixed" ] }
#-------------------------------------------------------------------------------

def fetch_repology_problems():
    """
    Retrieves repology problems for the 'slackbuilds' repo.
    Returns the fetched data.
    """
    repo = "slackbuilds"
    repologyAPI = "https://repology.org/api/v1/repository/{:s}/problems".format(repo)

    # The API spec doesn't say whether the 'problems' result set is is chunked.
    # In practice it seems to be truncated at 500 problems, so let's not go there.
    return requests.get(repologyAPI).json()

#-------------------------------------------------------------------------------

def process_repology_problems(rp_data):
    """
    Reads the raw Repology problems data 'rp_data'.
    Returns a dict suitable for merging into the 'Problems' global list.
    """
    logging.debug("started")

    rp_problems = {}

    for p in rp_data:

        prgnam = p["name"]
        catnam = globals.SBinfo[prgnam]["catnam"]
        maintainer = globals.SBinfo[prgnam]["maintainer"]
        email = globals.SBinfo[prgnam]["email"]

        # All Repology problems currently start with "Homepage link" and
        # a URL, but we'll classify any that don't as "other". To make the
        # included URL clickable we need to remove the surrounding quotes.
        hstr = "Homepage link \""
        if p["problem"].startswith(hstr):
            problemtype = "homepage"
            description = p["problem"][len(hstr):].replace("\" "," ",1)
        else:
            problemtype = "other"
            description = p["problem"]

        rp_problems[prgnam].update({  "type":   problemtype,
                                      "desc":   description,
                                      "source": "repology",
                                      "since":  data_updated.strftime("%Y-%m-%d"),
                                      "status": "new"  })

    logging.debug("finished -- {:d} repology problems".format(len(rp_problems)))
    return rp_problems

#-------------------------------------------------------------------------------

def merge_problems(problems):
    """
    Merge 'problems' into 'Problems'.
    """
    for prgnam, problist in problems.items():
        if prgnam not in Problems:
            Problems.update( {prgnam,problist} )
        else:
            newproblist = Problems[prgnam]
            for p in problist:
                if p not in newproblist:
                    newproblist.append(p)
            Problems.update( {prgnam,newproblist} )

#-------------------------------------------------------------------------------

def load_repology_problems():
    """
    Load repology problems into 'Problems'.
    """
    logging.info("started")

    rp_data = utils.load( "repology_problems.p",
                          globals.upd_repgy,
                          fetch_repology_problems
                          )

    rp_problems = process_repology_problems(rp_data)
    probcount = len(rp_problems)
    stats.setStats("problems.repology.count",probcount)

    merge_problems(rp_problems)
    stats.addStats("problems.count",probcount)

    logging.info("finished -- {:d} repology problems".format(probcount))

#-------------------------------------------------------------------------------

def load_download_problems():
    pass

#-------------------------------------------------------------------------------

def load_security_problems():
    pass

#-------------------------------------------------------------------------------

def load_build_problems():
    pass

#-------------------------------------------------------------------------------

def load_problems():
    """
    Run all the "load" functions.
    """
    for f in ( load_repology_problems,
               load_download_problems,
               load_security_problems,
               load_build_problems
               ):
        f()

#-------------------------------------------------------------------------------

def render_problems_report():
    """
    Render the problems report page.
    """
    logging.info("started")

    utils.renderer( page_subdir="reports",
                    page_name="problems.html",
                    page_title="SBo Problems Report",
                    TemplateData=globals.Problems )

    logging.info("finished")

#-------------------------------------------------------------------------------
