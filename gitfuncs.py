"""
SBo Dashboard
gitfuncs.py
Functions for accessing the local SlackBuilds.org clone

David Spencer 2018
See LICENCE for copyright information
"""

import os
import subprocess

import globals

#-------------------------------------------------------------------------------

def getnewest(repopath,itempath):
    """
    Return git log information about the newest commit for 'itempath'
    (this is painfully slow)
    """
    glog = subprocess.run(
        "git log --pretty=\"format:%ai %h %s\" -n 1 {:s}".format(itempath),
        shell=True,
        cwd=repopath,
        check=True,
        stdout=subprocess.PIPE
        )

#   from pygit2 import Repository, GIT_SORT_TOPOLOGICAL, GIT_SORT_REVERSE
#   repo = Repository(globals.sbrepo+"/.git")
#   for commit in repo.walk(repo.head.target, GIT_SORT_TOPOLOGICAL):
#        break
#   return (commit.hash, commit.authordate, commit.message)

    return(glog.stdout.decode("utf-8"))

#-------------------------------------------------------------------------------

def clone_sbo():
    """
    Initial clone from SlackBuilds.org
    """
    subprocess.run(
        "git clone git://git.slackbuilds.org/slackbuilds.git {:s}".format(globals.sbdir),
        shell=True,
        cwd=globals.dashdir,
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL
        )

#-------------------------------------------------------------------------------

def pull_sbo():
    """
    Make sure we're on 'master' and do a fast forward pull from origin
    """
    subprocess.run(
        "git checkout master ; git pull --ff-only origin",
        shell=True,
        cwd=globals.sbdir,
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL
        )

#-------------------------------------------------------------------------------

def megalog(SEP):
    """
    This produces log lines of the form
      dateSEPcommitSEPnameSEPemail
    where
      %ad date    = YYYY-MM-DD (we don't care about the time)
      %h  commit  = abbreviated SHA1
      %aN author  = author name, contains spaces
      %aE email   = author email, can contain spaces :(
    (see man git-log)
    """
    gitlog = subprocess.run(
        "git log --format=\"%ad{0:s}%h{0:s}%aN{0:s}%aE\" --date=\"format:%Y-%m-%d\"".format(SEP),
        shell=True,  # Unsafe? phooey.
        cwd=globals.sbdir,
        check=True,
        stdout=subprocess.PIPE
        )
    return gitlog.stdout
