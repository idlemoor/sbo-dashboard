"""
SBo Dashboard
stats.py
Statistics: global variables, persistent storage and utility functions

David Spencer 2018
See LICENCE for copyright information
"""

import os
import datetime
import pickle
import logging

import globals

#-------------------------------------------------------------------------------
# Data structures

# Stats is a key-value store for statistics gathered in the current run
#   Stats = { statsname: statsvalue }
#
Stats = {}

# Historical Stats structures are stored in StatsHistory, keyed on updateref
#   StatsHistory = { updateref: { Stats } }
#
StatsHistory = {}

#-------------------------------------------------------------------------------
# Initialise StatsHistory from storage

stats_path = os.path.join(globals.datadir,"StatsHistory.p")

try:
    with open(stats_path,"rb") as stats_file:
        StatsHistory = pickle.load(stats_file)
except:
    # no storage => this is the first ever run, so don't error
    pass

#-------------------------------------------------------------------------------
# Handy functions

def setStats(key,n=0):
    """
    Set Stats[key] to 'n', default 0
    """
    global Stats, StatsHistory
    Stats[key] = n
    return

def addStats(key,n=1):
    """
    Add 'n' to Stats[key], or set it if key doesn't exist
    """
    global Stats, StatsHistory
    if key in Stats:
        Stats[key] += n
    else:
        Stats[key] = n
    return

def getStats(key):
    """
    Get Stats[key], returning 0 when key doesn't exist
    """
    global Stats, StatsHistory
    if key in Stats:
        return Stats[key]
    else:
        return 0

#-------------------------------------------------------------------------------
# Update and save the StatsHistory structure.
# This is called from 'sbodash' just before the end of the run.

def saveStatsHistory():
    """
    Expire old stats history and insert new stats
    """
    global Stats, StatsHistory
    expdate = globals.updateref - datetime.timedelta(days=globals.keepstats)
    for olddate in StatsHistory:
        if olddate < expdate:
            del StatsHistory[olddate]
    StatsHistory.update({globals.updateref: Stats})
    with open(stats_path,"wb") as stats_file:
         pickle.dump(StatsHistory, file=stats_file)
    return
