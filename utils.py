"""
SBo Dashboard
utils.py
Utility functions

David Spencer 2018
See LICENCE for copyright information
"""

import os
import logging
import pickle
import jinja2

import globals
import stats

#-------------------------------------------------------------------------------

def unspamtrap(address):
    """
    Remove typical "anti-spam" obfuscations from an email address
    """

    fixedaddress = address
    fixedaddress = fixedaddress.replace(".nospam","")
    fixedaddress = fixedaddress.replace("(removeNOandSPAM)","")
    fixedaddress = fixedaddress.replace("SPAM","")
    fixedaddress = fixedaddress.replace(" at ","@")
    fixedaddress = fixedaddress.replace("<at>","@")
    fixedaddress = fixedaddress.replace("_at_","@")
    fixedaddress = fixedaddress.replace("-at-","@")
    fixedaddress = fixedaddress.replace("{at}","@")
    fixedaddress = fixedaddress.replace("~at~","@")
    fixedaddress = fixedaddress.replace("(at)","@")
    fixedaddress = fixedaddress.replace("[at]","@")
    fixedaddress = fixedaddress.replace("[AT]","@")
    fixedaddress = fixedaddress.replace("AT","@")
    fixedaddress = fixedaddress.replace("[@]","@")
    fixedaddress = fixedaddress.replace("{@}","@")
    fixedaddress = fixedaddress.replace("_@_","@")
    fixedaddress = fixedaddress.replace(" dot ",".")
    fixedaddress = fixedaddress.replace("<dot>",".")
    fixedaddress = fixedaddress.replace("[dot]",".")
    fixedaddress = fixedaddress.replace("[DOT]",".")
    fixedaddress = fixedaddress.replace("DOT",".")
    fixedaddress = fixedaddress.replace("[.]",".")
    fixedaddress = fixedaddress.replace("{.}",".")
    fixedaddress = fixedaddress.replace("{dot}",".")
    fixedaddress = fixedaddress.replace("(dot)",".")
    fixedaddress = fixedaddress.replace("~dot~",".")
    fixedaddress = fixedaddress.replace(" ","")
    fixedaddress = fixedaddress.lower()
    return(fixedaddress)

#-------------------------------------------------------------------------------
# Render a page - this is a neat and tidy interface to Jinja2

def renderer( page_subdir="",
              page_name="index.html",
              page_title="",
              template="",
              TemplateData=None ):
    """
    Render the template to 'sitedir/page_subdir/page_name'.
    The page title will be 'page_title' (duh).
    'TemplateData' is passed from the caller to the template.
    'Stats' is supplied to every template.
    """

    if page_title == "":
        if page_name.endswith(".html"):
            page_title = page_name[:-5].capitalize()
        else:
            page_title = page_name.capitalize()
    if template == "":
        if page_name.endswith(".html"):
            template = page_name[:-5]
        else:
            template = page_name

    page_path = os.path.join(globals.sitedir,page_subdir,page_name)
    os.makedirs(os.path.dirname(page_path),exist_ok=True)

    jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(globals.templdir),autoescape=None)
    mytemplate = jenv.get_template(template+".jinja2")

    with open(page_path,"w",encoding="utf-8") as page_file:
        print(
            mytemplate.render(
                siteurl=globals.siteurl,
                page_title=page_title,
                page_datetime=globals.updateref.strftime("%Y-%m-%d %T %Z"),
                TemplateData=TemplateData,
                Stats=stats.Stats,
                ident=globals.ident
                ),
            file=page_file
            )

#-------------------------------------------------------------------------------
# Persistent storage

def load(filename, max_age=maxage, fetch=fetch_function):
    """
    Return an up-to-date object synced with persistant storage.
    If 'filename' does not exist or is too old, call 'fetch_function' to refresh
    the object (eg, by getting it from the net) and write it to 'filename'.
    Otherwise, just load the object from 'filename'.
    """

    data_path = os.path.join(globals.datadir,filename)
    if data_path.endswith(".p"):
        timestamp_path = data_path[0:-2]+"_updated.p"
    else:
        timestamp_path = data_path+"_updated.p"

    try:
        with open(timestamp_path,"rb") as timestamp_file:
            prev_updated = pickle.load(timestamp_file)
    except FileNotFoundError:
            prev_updated = None

    if (    not os.path.isfile(data_path) or
            prev_updated is None or
            prev_updated + datetime.timedelta(days=maxage) < globals.updateref
            ):
        data = fetch_function()
        with open(data_path,"wb") as data_file:
            pickle.dump(data, file=data_file)
        with open(timestamp_path,"wb") as timestamp_file:
            pickle.dump(globals.updateref, file=timestamp_file)

    else:
        with open(data_path,"rb") as data_file:
            data = pickle.load(data_file)

    return data

#-------------------------------------------------------------------------------
