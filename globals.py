"""
SBo Dashboard
globals.py
Initialise global variables

David Spencer 2018
See LICENCE for copyright information
"""

import os
import sys
import datetime
import configparser
import logging

import gitfuncs

#-------------------------------------------------------------------------------
# For details of these global structures, see the relevant modules. They
# contain data correlated from multiple sources, so for example, to get info
# about the maintainers from Repology, we need to have a list of maintainers,
# and that list can only come from the cloned SlackBuilds repo. And that is
# why these structures are globals...

SBinfo = {}
SBcatindex = {}
SBmntindex = {}

Maintainerinfo = {}
Maintainerindex = {}

Problems = {}

#-------------------------------------------------------------------------------
# Create a timestamp for this run

updateref = datetime.datetime.now(datetime.timezone.utc)

#-------------------------------------------------------------------------------
# Create the configuration variables

def configulator(inidict, varname, defaultvalue=""):
    """
    Get config values from environment and from a configparser ini dict
      * environment overrides the ini dict
      * default value is substituted if unset *or empty*
    """
    value = os.environ.get( varname.upper(), inidict.get(varname,defaultvalue) )
    if value == "":
        value = defaultvalue
    return value

config = configparser.ConfigParser()
config.read("sbodash.ini")

ini_locations = config["Locations"]
dashdir  = configulator( ini_locations, "dashdir",  os.getcwd() )
templdir = configulator( ini_locations, "templdir", "templates" )
datadir  = configulator( ini_locations, "datadir",  "data"      )
sbdir    = configulator( ini_locations, "sbdir",    "data/slackbuilds" )
sitedir  = configulator( ini_locations, "sitedir",  "site"      )
siteurl  = configulator( ini_locations, "siteurl",  "http://localhost/sbodash" )

ini_statistics = config["Statistics"]
keepstats = int( configulator( ini_statistics, "keepstats",  "366" ) )

ini_logging = config["Logging"]
logsdir  = configulator( ini_logging, "logsdir",  "logs" )
loglevel = configulator( ini_logging, "loglevel", "INFO" ).upper()

ini_updates = config["Updates"]
upd_sbo   = int( configulator( ini_updates, "upd_sbo",   "7" ) )
upd_repgy = int( configulator( ini_updates, "upd_repgy", "7" ) )

# Fix up paths
if not os.path.isabs(dashdir):
    os.path.join(os.getcwd(),dashdir)
if not os.path.isabs(templdir):
    templdir = os.path.join(dashdir,templdir)
if not os.path.isabs(datadir):
    datadir = os.path.join(dashdir,datadir)
if not os.path.isabs(sbdir):
    sbdir = os.path.join(dashdir,sbdir)
if not os.path.isabs(sitedir):
    sitedir = os.path.join(dashdir,sitedir)
if not os.path.isabs(logsdir):
    logsdir = os.path.join(dashdir,logsdir)

# These directories must exist
if not os.path.isdir(dashdir):
    sys.exit("Not a directory: {:s}".format(dashdir))
if not os.path.isdir(templdir):
    sys.exit("Not a directory: {:s}".format(templdir))

# These directories will be created if they do not exist
os.makedirs(datadir,exist_ok=True)
os.makedirs(sbdir,  exist_ok=True)
os.makedirs(sitedir,exist_ok=True)
os.makedirs(logsdir,exist_ok=True)

# The loglevel must be a valid loglevel
if loglevel not in ( "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL" ):
    sys.exit("Not a valid log level: {:s}".format(loglevel))

#-------------------------------------------------------------------------------
# Get our version identifier

ident = "sbodash {:s}".format(gitfuncs.getnewest(dashdir,".").split(" ")[3])

#-------------------------------------------------------------------------------
# Set up logging, and log some debug info

logging.basicConfig( filename=os.path.join(logsdir,"sbodash_{:s}.log".format(updateref.strftime("%Y-%m-%d"))),
                     format="%(asctime)s %(funcName)s %(message)s",
                     level=getattr(logging,loglevel)
                     )

logging.debug(ident)
logging.debug("configuration:")
logging.debug("  {:s} = {:s}".format("updateref",updateref.strftime("%Y-%m-%d %T")))
logging.debug("  {:s} = {:s}".format("dashdir",  dashdir))
logging.debug("  {:s} = {:s}".format("templdir", templdir))
logging.debug("  {:s} = {:s}".format("datadir",  datadir))
logging.debug("  {:s} = {:s}".format("sbdir",    sbdir))
logging.debug("  {:s} = {:s}".format("sitedir",  sitedir))
logging.debug("  {:s} = {:s}".format("siteurl",  siteurl))
logging.debug("  {:s} = {:d}".format("keepstats",keepstats))
logging.debug("  {:s} = {:s}".format("logsdir",  logsdir))
logging.debug("  {:s} = {:s}".format("loglevel", loglevel))

#-------------------------------------------------------------------------------
