# SlackBuilds.org Dashboard

Periodically create a set of web pages reporting on SlackBuilds.org

## External requirements

  python3
  python-requests
  python-chardet
  idna
  python-certifi
  Jinja2
  MarkupSafe
  libgit2
  pygit2

## Running

 * Clone this repository
 * Check sbodash.ini and edit it as required
 * Run ./sbodash
 * Make your webserver serve the 'site' subdirectory
 * Re-run ./sbodash periodically (e.g. once a week)
