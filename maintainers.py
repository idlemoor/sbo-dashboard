"""
SBo Dashboard
maintainers.py
Functions and data structures relating to maintainers

David Spencer 2018
See LICENCE for copyright information
"""

import sys
import os
import subprocess
import json
import collections
import datetime
import logging
import urllib
import requests

import globals
import stats
import utils

#-------------------------------------------------------------------------------
# Maintainerinfo[primary] = { "mntnams": ["secondary1","secondary2"...],
#                             "emails":  ["email1","email2"...],
#                             "latest":  { "date":cdate, "commit":commit } }

#-------------------------------------------------------------------------------

def setlatest(mntnam,cdate,commit):
    """
    Record commit details into globals.Maintainerinfo[mntnam]
    """
    if "latest" not in globals.Maintainerinfo[mntnam].keys() or globals.Maintainerinfo[mntnam]["latest"]["date"] < cdate:
        globals.Maintainerinfo[mntnam]["latest"] = {"date":cdate, "commit":commit}

#-------------------------------------------------------------------------------

def fetch_maintainers():
    """
    Retrieves maintainer metrics from repology.
    Returns the fetched data.
    """

    repologyAPI = "https://repology.org/api/v1/metapackages/"
    sess = requests.session()

    maintainer_data = {}

    for rawemail, mntinfo in globals.SBmntindex:
        total = 0; newest = 0; outdated = 0
        if not mntinfo["redact"]:
            email = mntinfo["email"]
            filters = {"maintainer": email}
            metapkglist = collections.OrderedDict()
            metapkgchunk = sess.get(repologyAPI,params=filters).json()
            if len(metapkgchunk) != 0:
                metapkglist.update(metapkgchunk)
                while len(metapkgchunk) == 200:
                    lastkey,lastvalue = metapkglist.popitem()
                    # re-add the popped item -- the API resultset will start with the *next* item :(
                    metapkglist.update({lastkey:lastvalue})
                    metapkgchunk = sess.get(repologyAPI+lastkey+"../",params=filters).json()
                    metapkglist.update(metapkgchunk)
                for metapkgname, repolist in metapkglist.items():
                    for repopkg in repolist:
                        if repopkg["repo"] == "slackbuilds" and repopkg["maintainers"] == [email]:
                            total += 1
                            if repopkg["status"] == "newest":
                                newest += 1
                            elif repopkg["status"] == "outdated":
                                outdated += 1

        maintainer_data.update({ "email": email,
                                 "mntinfo": {  "total": total,
                                               "newest": newest,
                                               "outdated": outdated
                                               }
                                  })

    return maintainer_data

#-------------------------------------------------------------------------------

def process_maintainers(maintainerdata):
    """
    Merges the Repology maintainer data and the Slackbuilds maintainer data
    into the global Maintainerinfo dict.
    Returns nothing.
    """
    logging.info("started")

    # (1) Correlate all the names and all the emails of each
    #     maintainer in the following dictionaries:
    #
    # Maintainerinfo[primary] = { "mntnams":  ["secondary1","secondary2"...],
    #                             "emails": ["email1","email2"...],
    #                             "latest": { "date":cdate,"commit":commit } }
    # where 'primary' is the primary maintainer name,
    # and 'secondary' is any other name we find that shares an email with
    #                    the primary name or any of its other secondary names.
    #
    # Ideally there would be only one name for each maintainer, but people
    # are fickle. The primary name is just the first variant to be discovered.
    # It might be obsolete or a typo, but we don't care :)
    #
    mntnam_lookup = {}
    #   mntnam_lookup[secondary] = "primary"
    #
    email_lookup = {}
    #   email_lookup[email] = "primary"

    logging.debug("correlating maintainers and emails")

    for rawemail, mntinfo in globals.SBmntindex.items():
        email = mntinfo["email"]
        for mntnam in mntinfo["mntnamlist"]:
            if mntnam not in mntnam_lookup and email not in email_lookup:
                # completely new mntnam+email combo
                maintainer = mntnam
                mntnam_lookup[mntnam] = maintainer
                email_lookup[email] = maintainer
                globals.Maintainerinfo[maintainer] = {  "mntnams": [maintainer],
                                                        "emails": [email],
                                                        "slackbuilds": mntinfo["prgnamlist"]
                                                        }
            elif mntnam in mntnam_lookup and email not in email_lookup:
                # mntnam is known, so it's a variant email for an existing maintainer
                maintainer = mntnam_lookup[mntnam]
                email_lookup[email] = maintainer
                globals.Maintainerinfo[maintainer]["emails"].append(email)
                globals.Maintainerinfo[maintainer]["slackbuilds"].extend(mntinfo["prgnamlist"])
            elif mntnam not in mntnam_lookup and email in email_lookup:
                # email is known, so it's a variant name for an existing maintainer
                maintainer = email_lookup[email]
                mntnam_lookup[mntnam] = maintainer
                globals.Maintainerinfo[maintainer]["mntnams"].append(mntnam)
                globals.Maintainerinfo[maintainer]["slackbuilds"].extend(mntinfo["prgnamlist"])
            else: # mntnam in mntnam_lookup and email in email_lookup:
                #   we know both the mntnam and the email,
                #   but do we need to merge two Maintainerinfo records into one?
                maintainer = mntnam_lookup[mntnam]
                synonym = email_lookup[email]
                if maintainer != synonym:
                    # yes, we need to merge them :(
                    globals.Maintainerinfo[maintainer]["mntnams"].extend(globals.Maintainerinfo[synonym]["mntnams"])
                    globals.Maintainerinfo[maintainer]["emails"].extend(globals.Maintainerinfo[synonym]["emails"])
                    globals.Maintainerinfo[maintainer]["slackbuilds"].extend(globals.Maintainerinfo[synonym]["slackbuilds"])
                    globals.Maintainerinfo[maintainer]["slackbuilds"].extend(mntinfo["prgnamlist"])
                    for k in mntnam_lookup.keys():
                        if mntnam_lookup[k] == synonym:
                            mntnam_lookup[k] = maintainer
                    for k in email_lookup.keys():
                        if email_lookup[k] == synonym:
                            email_lookup[k] = maintainer
                    del globals.Maintainerinfo[synonym]
                else:
                    # we only need to extend the SlackBuild list
                    globals.Maintainerinfo[maintainer]["slackbuilds"].extend(mntinfo["prgnamlist")

    stats.addStats("maintainers.count",len(globals.Maintainerinfo.keys()))

    # (2) Go through the entire git history to get each maintainer's latest update.
    # We do all this because the most recent commit for any given SlackBuild
    # might well not be the maintainer.

    logging.debug("getting commit history")

    # use a crazy separator for the 'git log' columns that hopefully isn't in any commit message
    SEP = "+|+"
    gitlog = gitfuncs.megalog(SEP)

    for rawline in gitlog.splitlines():
        try:
           logline = rawline.decode("utf8")
        except:
           logline = rawline.decode("latin-1")
        date, commit, author, email = logline.split(SEP)

        if author in mntnam_lookup:
            setlatest(mntnam_lookup[author],date,commit)
        elif email in email_lookup:
            setlatest(email_lookup[email],date,commit)
        # Ignore git authors that are in the log but not in mntnam_lookup or email_lookup.
        # They are either retired maintainers, or randoms with a nice git mention :)

    # (3) Add metrics from the repology data and the commit data
    # and format some html table cells.

    # Date since which a maintainer is "recent"
    recentdays = 180
    recentdate = (globals.updateref - datetime.timedelta(days=recentdays)).strftime("%Y-%m-%d")
    # Date before which a maintainer is "inactive" (hardcoded release date of 14.2)
    inactivedate = "2016-06-30"

    for mntnam, mntdeets in globals.Maintainerinfo.items():

        mntdeets["tdmaintainer"] = mntnam
        mntdeets["mntnum"] = mntnum ; mntnum += 1
        mntdeets["tdslackbuilds"] = "{:d}".format(len(mntdeets["slackbuilds"]))

        mntdeets["status"] = "inactive"
        if "latest" in mntdeets:
            mntdeets["tdlatest"] = "<a class=\"c\" target=\"_blank\" href=\"https://git.slackbuilds.org/slackbuilds/commit/?id={:s}\">{:s}</a>".format(mntdeets["latest"]["commit"],mntdeets["latest"]["date"])
            if mntdeets["latest"]["date"] >= inactivedate:
                mntdeets["status"] = "active"
                stats.addStats("maintainers.active")
                if mntdeets["latest"]["date"] >= recentdate:
                    mntdeets["status"] = "recent"
                    stats.addStats("maintainers.recent")
        else:
            mntdeets["tdlatest"] = "Never"

        repology_slackbuilds = 0; repology_newest = 0; repology_outofdate = 0
        for email in mntdeets["emails"]:
            count, new, old = repology_by_maintainer(email,sess)
            repology_slackbuilds += count; repology_newest += new; repology_outofdate += old
            stats.addStats("slackbuilds.uptodate",new); stats.addStats("slackbuilds.outofdate",old)
        if repology_slackbuilds != 0:
            mntdeets["tdmaintainer"] += "<br><a class=\"e\" target=\"_blank\" href=\"http://repology.org/metapackages/?maintainer={:s}\">{:s}</a>".format(urllib.parse.quote(email),email)
            mntdeets["tdnewest"] = "{:d}".format(repology_newest)
            mntdeets["tdnewestpct"] = "{:.1f}%".format(repology_newest*100/repology_slackbuilds)
            mntdeets["tdoutofdate"] = "{:d}".format(repology_outofdate)
            mntdeets["tdoutofdatepct"] = "{:.1f}%".format(repology_outofdate*100/repology_slackbuilds)
        else:
            mntdeets["tdmaintainer"] += "<br><span class=\"e\">{:s}</span>".format(email)
            mntdeets["tdnewest"] = "-"
            mntdeets["tdnewestpct"] = "-"
            mntdeets["tdoutofdate"] = "-"
            mntdeets["tdoutofdatepct"] = "-"

    stats.setStats("maintainers.inactive",stats.getStats("maintainers.active")-stats.getStats("maintainers.recent"))

    logging.info("finished -- ${:d} maintainers".format(mntnum))

#-------------------------------------------------------------------------------

def load_maintainers():
    """
    Load maintainers into 'Maintainerinfo' and 'Maintainerindex' globals.
    """
    logging.info("started")

    maintainers_data = utils.load( "maintainers_data.p",
                                   globals.upd_repgy,
                                   fetch_maintainers
                                   )

    process_maintainers(repology_problem_data)
    stats.setStats("maintainers.count",len(globals.Maintainerinfo))

    logging.info("finished")

#-------------------------------------------------------------------------------

def render_maintainers_report():
    """
    Render the maintainers report page
    """
    logging.info("started")

    utils.renderer( page_subdir="reports",
                    page_name="maintainers.html",
                    page_title="SBo Maintainers Report",
                    TemplateData=globals.Maintainerinfo )

    logging.info("finished")

#-------------------------------------------------------------------------------

def render_maintainers_detail():
    """
    Render all the maintainer detail pages
    """
    logging.info("started")

    for mntnam, mntinfo in globals.Maintainerinfo.items():

        email_list = []
        newest_commit = []
        slackbuild_list = []
        problem_list = []

        MaintainerDetail = { "email_list":      email_list,
                             "newest_commit":   newest_commit,
                             "slackbuild_list": slackbuild_list,
                             "problem_list":    problem_list
                             }

        utils.renderer( page_subdir="maintainers",
                        page_name=email,
                        page_title=mntnam,
                        template="maintainers_detail",
                        TemplateData=MaintainerDetail
                        )

    logging.info("finished")

#-------------------------------------------------------------------------------
