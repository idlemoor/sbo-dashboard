"""
SBo Dashboard
support.py
Create the site index and support files, update the slackbuilds

David Spencer 2018
See LICENCE for copyright information
"""

import os
import datetime
import jinja2
import subprocess
import logging

import globals
import utils

#-------------------------------------------------------------------------------

def render_indexes():
    """
    Render the index pages and the homepage
    """
    logging.info("started")

    utils.renderer( page_subdir="maintainers",
                    page_title="SBo Maintainers Index",
                    template="maintainers_index",
                    TemplateData=globals.Maintainerindex
                    )

    utils.renderer( page_subdir="slackbuilds",
                    page_title="SBo SlackBuilds Index",
                    template="slackbuilds_index",
                    TemplateData=globals.SBcatindex
                    )

    # build logs -- for now, just create an empty directory
    os.makedirs(os.path.join(globals.sitedir,"buildlogs"),exist_ok=True)

    utils.renderer( page_title="SBo Dashboard",
                    template="homepage"
                    )

    logging.info("finished")

#-------------------------------------------------------------------------------

def render_support():
    """
    Copy the support files into the site's support directory
    """
    logging.info("started")
    subprocess.run(
        "cp -a static/robots.txt {:s}/robots.txt".format(globals.sitedir),
        shell=True,
        cwd=globals.dashdir,
        check=True,
        stdout=subprocess.DEVNULL
        )
    subprocess.run(
        "rsync -a --delete static/support/ {:s}/support/".format(globals.sitedir),
        shell=True,
        cwd=globals.dashdir,
        check=True,
        stdout=subprocess.DEVNULL
        )
    logging.info("finished")

#-------------------------------------------------------------------------------
