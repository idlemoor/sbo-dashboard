"""
SBo Dashboard
slackbuilds.py
Functions and dicts for a local SlackBuilds.org clone

David Spencer 2018
See LICENCE for copyright information
"""

import os
import re
import glob
import subprocess
import logging
import datetime

import gitfuncs
import globals
import stats
import utils

#-------------------------------------------------------------------------------
# SBinfo = { prgnam    }
#
# lookup catnam, get a list of prgnams
# SBcatindex[catnam] = [ prgnam1, prgnam2... ]
#
# lookup maintainer email, get list of prgnams, names, etc
# SBmntindex[rawemail] = { "email": email,
#                          "redact": redact,
#                          "mntnamlist": [mntnam1, mntnam2...],
#                          "prgnamlist": [prgnam1, prgnam2...] }
#-------------------------------------------------------------------------------

def fetch_slackbuilds():
    """
    Create or update a clone of the SBo repo.
    Returns a dictionary of information about each prgnam in the repo,
    suitable for loading directly into the SBinfo global.
    """
    logging.info("started")

    sbdata = {}

    if os.path.isfile(os.path.join(globals.sbdir,"ChangeLog.txt")):
        logging.info("pulling from SBo repo")
        gitfuncs.pull_sbo()
    else:
        logging.info("cloning from SBo repo")
        gitfuncs.clone_sbo()

    logging.debug("reading .info files")

    # These regexes are used >7000 times, so we might as well compile them.
    re_version = re.compile(r"VERSION=\".*\"")
    re_homepage = re.compile(r"HOMEPAGE=\".*\"")
    re_maint = re.compile(r"MAINTAINER=\".*\"")
    re_email = re.compile(r"EMAIL=\".*\"")

    infocount = 0
    for infofile in sorted(glob.glob(globals.sbdir+"/*/*/*.info")):
        infocount += 1
        logging.debug(infofile)

        catnam = infofile.split("/")[-3]
        prgnam = infofile.split("/")[-2]

        try:
            with open(infofile, "r", encoding="utf-8") as openinfo:
                info = openinfo.read()
        except:
            with open(infofile, "r", encoding="latin-1") as openinfo:
                info = openinfo.read()

        versionarray = re_version.findall(info)
        homepagearray = re_homepage.findall(info)
        maintainerarray = re_maint.findall(info)
        emailarray = re_email.findall(info)
        if len(versionarray) == 1 and len(homepagearray) == 1 and len(maintainerarray) == 1 and len(emailarray) == 1:
            version = versionarray[0][9:-1]
            homepage = homepagearray[0][10:-1]
            maintainer = maintainerarray[0][12:-1]
            email = emailarray[0][7:-1]
        else:
            # We're not here to validate the .info files.
            # If there's not exactly one MAINTAINER and EMAIL, bollocks to it.
            logging.warn("{:s} has missing or broken or duplicated entries".format(infofile))
            continue

        gitnewest = gitfuncs.getnewest(sbodir,catnam+"/"+prgnam)

        sbdata.update({ prgnam: { "catnam": catnam,
                                  "version": version,
                                  "homepage": homepage,
                                  "maintainer": maintainer,
                                  "email": email,
                                  "gitnewest": gitnewest
                                  } })

    logging.info("finished -- {:d} info files".format(infocount))
    return sbdata

#-------------------------------------------------------------------------------

def process_slackbuilds(sbdata):
    """
    Populates the SBinfo and associated globals from the supplied data structure.
    Returns nothing.
    """
    logging.debug("started")

    # (1) create SBinfo
    globals.SBinfo = sbdata
    # well, that was easy :p

    # (2) create SBprgindex and SBcatindex
    globals.SBprgindex = {}
    globals.SBcatindex = {}

    for prgnam, prginfo in sbdata.items():
        catnam = prginfo["catnam"]
        globals.SBprgindex.update({prgnam: catnam})
        if catnam not in globals.SBcatindex:
            globals.SBcatindex.update({catnam: [prgnam]})
        else:
            globals.SBcatindex[catnam].append(prgnam)

    if len(globals.SBprgindex) != len(globals.SBinfo):
        logging.warn("{:d} duplicate prgnams in slackbuilds repo".format(infocount-len(globals.SBinfo)))

    # (3) create SBmntindex
    globals.SBmntindex = {}

    for prgnam, prginfo in sbdata.items():

        rawemail = prginfo["email"]
        email = utils.unspamtrap(rawemail)
        redact = email != rawemail

        if rawemail in SBmntindex:
            SBmntindex[rawemail]["prgnamlist"].append(prgnam)
            if mntnam not in SBmntindex[rawemail]["mntnamlist"]:
                SBmntindex[rawemail]["mntnamlist"].append(mntnam)
        else:
            SBmntindex.update(  {   rawemail,
                                    { "email": email, "redact": redact,
                                      "mntnamlist": [mntnam],
                                      "prgnamlist": [prgnam] }
                                } )

    logging.debug("finished -- {:d} slackbuilds".format(prgnamcount))

#-------------------------------------------------------------------------------

def load_slackbuilds():
    """
    Load the slackbuilds data.
    """
    logging.info("started")

    slackbuilds_data = utils.load( "slackbuilds_data.p",
                                   globals.upd_sbo,
                                   fetch_slackbuilds
                                   )

    process_slackbuilds(slackbuilds_data)
    stats.setStats("slackbuilds.count",len(slackbuilds_data))

    logging.info("finished")

#-------------------------------------------------------------------------------

def render_slackbuilds_detail():
    """
    Render all the slackbuild detail pages
    """
    logging.info("started")

    for prgnam, prginfo in globals.SBinfo.items():
        catnam = prginfo["catnam"]
        logging.debug(catnam+"/"+prgnam)
        gitnewest = prginfo["gitnewest"]
        commitinfo = gitnewest.split(" ")
        commitdate = " ".join(commitinfo[0:3])
        commithash = commitinfo[3]
        commitsubj = " ".join(commitinfo[4:])
        SlackbuildDetail = { "prgnam": prgnam,
                             "catnam": catnam,
                             "version": prginfo["version"],
                             "homepage": prginfo["homepage"],
                             "maintainer": prginfo["maintainer"],
                             "email": prginfo["email"],
                             "commitdate": commitdate,
                             "commithash": commithash,
                             "commitsubj": commitsubj
                             }
        utils.renderer( page_subdir="slackbuilds/"+catnam,
                        page_name=prgnam,
                        page_title=catnam+"/"+prgnam,
                        template="slackbuilds_detail",
                        TemplateData=SlackbuildDetail
                        )

    logging.info("finished")

#-------------------------------------------------------------------------------
